"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import flask
from flask import request, render_template, redirect, url_for
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route("/display")
def display():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('display.html', items=items)

@app.route('/new', methods=['POST'])
def submit():

    db.tododb.remove({})
    # get km, location, open, close from html
    kms = request.form.getlist('km')
    locations = request.form.getlist('location')
    opens = request.form.getlist('open')
    closes = request.form.getlist('close')
    
    distance = request.form.get('distance')

    date = request.form.get('begin_date')
    time = request.form.get('begin_time')
    datetime = arrow.get(date+time, "YYYY-MM-DDHH:mm").isoformat()

    #app.logger.info(kms)
    #app.logger.info(locations)
    #app.logger.info(opens)
    #app.logger.info(closes)
    
    #create and submit entries to db
    for i in range(len(kms)):
        if kms[i] is not '':
            item_doc = {
                    'distance': distance,
                    'datetime': datetime,
                    'km': kms[i],
                    'location': locations[i],
                    'open': opens[i],
                    'close': closes[i]
                    }
            app.logger.info(item_doc)
            db.tododb.insert_one(item_doc)

    _items = db.tododb.find()
    items = [item for item in _items]
    #app.logger.info("finding db")
    #app.logger.info(not items)
    #app.logger.info(items is [])
    if not items: # there are no items in the list
        flask.abort(404, description="Must submit at least one control time")

    return redirect(url_for('index'))

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    #distance = flask.request.form("distance")
    #begin_date = flask.request.form("begin_date")
    #begin_time = flask.request.form("begin_time")

    app.logger.debug("Got a JSON request")
    km = request.args.get('km', type=float)
    date = request.args.get('dates', type=str)
    time = request.args.get('time', type=str)
    distance = request.args.get('distance', type=int)    

    print(date)
    print(time)
    print(distance)
    # print('\n')

    datetime = arrow.get(date+time, "YYYY-MM-DDHH:mm")

    print(datetime.isoformat())
    print('\n')


    print("km: " +str(km))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, distance, datetime)
    close_time = acp_times.close_time(km, distance, datetime)
    print(open_time)
    print(close_time)
    #item = {
    #        'km': km,
    #        'open_time':open_time,
    #        'close_time':close_time
    #}
    #db.tododb.insert_one(item)

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)
