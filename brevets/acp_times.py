"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

"""
current timetable for brevets

control location    minimum speed   maximum speed
0-200               15              34
200-400             15              32
400-600             15              30
600-1000            11.428          28
1000-1300           13.333          26
to note: 1300 is out of range of the largest brevet distance 1000
(can only be up to 1200 for 1000 brevet distance
"""
CONTROLS = [(0, 200, 15, 34), (200, 400, 15, 32), (400, 600, 15, 30), (600, 1000, 11.428, 28), (1000, 1300, 13.333, 26)]
# by order given: control min, control max, minimum speed, maximum speed
# functions need both the min and max for control locations as they must calculate the differnce

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """

    counter = 0 
    # counter counts number of hours past the certain time
    
    if control_dist_km == 0:
        return brevet_start_time.isoformat()

    for entry in CONTROLS:
        control_min, control_max, min_speed, max_speed = entry
        
        if control_dist_km > brevet_dist_km:
            control_dist_km = brevet_dist_km

        if control_min <= brevet_dist_km: #control points up to 1.2x past the brevet distance are allowed
            if control_max <= control_dist_km:
                counter += (control_max-control_min)/max_speed
                continue
            elif control_max > control_dist_km > control_min:
                counter += (control_dist_km - control_min)/max_speed
                continue
    
    # add counter hours to start time, then return new time in isoformat()
    counter = round(counter*60)
    final_time = brevet_start_time.shift(minutes=+counter)
    return final_time.isoformat()
    # return arrow.now().isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    # close time is given by the minimum speed
    # 15/kmhr for 0-600
    # 11.428 for 
    # i.e. if the checkpoint is at 150km, the close time will be
    # 10 hours after the inital start time
    # close time for 0km is 1 hour after open time

    counter = 0 
    # counter counts number of hours past the certain time
    
    # prests for non calculated times

    if control_dist_km == 0:
        final_time = brevet_start_time.shift(hours=+1)
        return final_time.isoformat()

    test = [(200, 13.5), (300, 20), (400, 27), (600, 40), (1000, 75)]
    for i in test:
        time,diff = i
        if (brevet_dist_km == time and control_dist_km == time):
            # as per specific rule on given rule website
            final_time = brevet_start_time.shift(hours=+diff)
            return final_time.isoformat()


    for entry in CONTROLS:
        control_min, control_max, min_speed, max_speed = entry
        
        if control_dist_km > brevet_dist_km:
            control_dist_km = brevet_dist_km

        if control_min <= brevet_dist_km: #control points up to 1.2x past the brevet distance are allowed
            if control_max <= control_dist_km:
                counter += (control_max-control_min)/min_speed
                continue
            elif control_max > control_dist_km > control_min:
                counter += (control_dist_km - control_min)/min_speed
                continue
    
    # add counter hours to start time, then return new time in isoformat()
    counter = round(counter*60)
    final_time = brevet_start_time.shift(minutes=+counter)
    return final_time.isoformat()
    # return arrow.now().isoformat()
