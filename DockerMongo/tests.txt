Tests for Buttons

Intended Button 

- When submitting any table, any other previous control points will be deleted from the database. This includes submitting an empty table. To test, submit some kind of entry within the table, or none at all, and click the display button to confer.
- When clicking on this button, it will redirect you to the display page. This page will show any control points from the last submission
- Once on the display page, clicking on "return to homepage" will return you to the control point input table page

Error Cases

- When clicking Submit Query with an empty control time table, the webiste will raise a 404
- When clicking on Display Results when first loading the page, the website will raise a 404
- When clicking on Display Results after submitting an empty table, the website will raise a 404
- When going to localhost:5000/display is accessed not through a button, an error will not occur, but it will not print any control times

